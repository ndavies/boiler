.. boiler documentation master file, created by
   sphinx-quickstart on Mon Oct 21 00:23:45 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to boiler's documentation!
==================================

Boiler is a library for building and using modular boilerplate code.
It allows you to do things such as::

    mkdir project
    boiler init

    boiler apply python.web.flask
    boiler apply python.web.sqlalchemy

    boiler apply python.web.flask.blueprint name=api route=/api/
    boiler apply python.web.flask.blueprint name=admin route=/admin/

    boiler apply javascript.requirejs
    boiler apply javascript.backbone

This would build a working flask application with SQLAlchemy and two blueprints for ``/admin`` and ``/api``. It will then install requireJS and backboneJS (as well as underscore and JQuery)

In addition to this you can use this after you have started building your application on top of the boilerplate. For example if you then later decided that you wished to also have twitters bootstrap for all your CSS
you could simply run::

    boiler apply css.bootstrap

and it will install twitter's bootstrap code into your page headers and put bootstrap into the javascript directory.

The documentation is split into the following categories:

#. :ref:`general-information` - common information about how boiler works
#. :ref:`usage` - how to use and apply existing templates
#. :ref:`template-building` - how to build your own templates

.. _general-information:

General Information
===================

Below is an overview of how boiler works, most of this information is required to either use or build templates effectively.

Template Searching
++++++++++++++++++

Boiler accepts a ``BOILER_PATH`` variable and uses it to find templates in the same way that Linux and Unix find binaries using ``PATH``.
In addition to this variable Boiler accepts the flag ``--template-dir`` which will be **prepended** to the list in ``BOILER_PATH``.

Template names are in the form of ``python.web.flask`` which will look for the template config for template flask in the folder ``python/web/flask`` inside each of the paths
listed, the first matching template is chosen.

Tree Structure
++++++++++++++

Boiler maintains the project folder by attaching templates to a tree structure when you apply a template to a folder.
The tree is maintained to mirror the folder structure of the project. Templates metadata is then attached to
the node for the folder it was applied to. For example if you apply template ``x`` to folder ``foo`` and template ``y`` to folder ``foo/bar`` the 
tree will be ``/ -> foo -> bar`` and the metadata for ``x`` is attached to ``/ -> foo`` and the metadata for ``y`` is attached to ``/ -> foo -> bar``

Names vs Types
++++++++++++++

In boiler all templates have a type (this is the dotted string used to reference the template in the boiler-templates folder; for example ``python.web.flask``).

In addition templates may also have an explicit name or nickname. This can be useful if you wish to have
multiple of the same template; typically with different parameters; applied to the same folder.

A template name will take priority whenever searching to match a dependency. As such a named template in the top level directory will take priority over a template
on a leaf node even if it has the same type as the template on the root. The exact process is described below.

Dependency Resolution
+++++++++++++++++++++

Boiler need to resolve two different types of dependancies. It needs to resolve the dependency on another template and also resolve dependencies for 
any parameters or blocks it may need.

Templates
^^^^^^^^^

In order to satisfy a template dependency boiler will first find the node associated with the target folder.
Then it will simply walk up the tree looking for any template with the name matching the dependency name.
If no template is found with that name it will then walk up the tree again searching for a template with type
matching the dependency name.

Parameters
^^^^^^^^^^

Parameters are resolved in a very similar way as above, the current node is first found, then Boiler will traverse up the tree until it finds a 
template that provides the parameter it needs with the correct name and type.

Parameters do not need to all be met by the same template or at the same level.

.. _usage:

Usage
=====

.. _template-building:

Template Building
=================

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

