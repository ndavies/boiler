import re

VALID_REQUIRE_TYPES = set(["dir", "block", "file"])
VALID_SECTIONS = set(["basic", "depends", "location", "requires", "export:.*", "depend:.*"])

VALID_SECTIONS = set(re.compile(s) for s in VALID_SECTIONS)

