import jinja2

class VariableSource(object):
    '''
        This class wraps a function that returns items given their name
        it buffers all the exceptions from missing items.

        This is used by StringReader so it can render a whole template
        and then give a report of all the errors at the end instead of
        it failing on the first error.

        You can get objects with either attribute access (v.item) or 
        dict like access (v['item']).

        This class will eat any exception that is of or inherits from the
        exception given. So it is advised to use a very specific exception.
    '''

    def __init__(self, variable_source, NotFoundException):
        self._variable_source = variable_source
        self._NotFoundException = NotFoundException
        self._fails = {}
        self._results = {}

    def _get_fails(self):
        '''
            Return a set of all the items that were asked for that failed
        '''
        return set(self._fails.keys())

    def _raise_any_errors(self):
        '''
            Raise an exception if any fails occured.
            
            This adds an attribute .fails to the exception it raises
            with the exception objects of each item that failed.

            This is actually a public method and the _ is so it will not
            mess with _get.
        '''
        if len(self._fails):
            error = self._NotFoundException("Failed to render string")
            error.fails = self._fails
            raise error
        return None

    def _get(self, item):
        if item not in self._results:
            try:
                self._results[item] = self._variable_source(item)
            except self._NotFoundException as e:
                self._fails[item] = e
                self._results[item] = "FAIL"

        return self._results[item]

    def __getitem__(self, item):
        return self._get(item)
            
    def __getattr__(self, item):
        return self._get(item)

class StringRender(object):
    '''
        This wrapper is used to render strings of different template language types.

        It requires a function that will look up a variable given its name and the class
        for the exception that will be raised if no error occurs
    '''

    def __init__(self, variable_source, NotFoundException):
        self._NotFoundException = NotFoundException
        self._variable_source = variable_source

    def _raw_render(self, variables, body):
        raise NotImplementedError("The Base Class cannot render. Use an inheriting class")

    def render(self, body):
        '''
            Render the given template body
        '''

        # get a new variables source each time so old errors dont get raised
        variables = VariableSource(self._variable_source, self._NotFoundException)

        # this wont fail because of self.variables
        result = self._raw_render(variables, body)

        # Raise an error if there where missing variables
        variables._raise_any_errors()

        return result

class PythonFormatStringRender(StringRender):

    def __init__(self, *args, **kwargs):
        super(PythonFormatStringRender, self).__init__(*args, **kwargs)

    def _raw_render(self, variables, body):
        return str(body) % variables

class JinjaStringRender(StringRender):
    '''
        The format for the Jinja2 templates are as follows:

        <# if x #>
            /BB*
                comment
            *BB/
            <@ y @>
        <# endif #>

        These will clash with microsoft T4 and F# operators 
        but they are silly languages anyway.

        Also it will unfortuantly clash with postgres <@
        contains operator. Im sorry Postgres :(
    '''
    
    def __init__(self, *args, **kwargs):
        super(JinjaStringRender, self).__init__(*args, **kwargs)
        
        self.jinja_environment = jinja2.Environment(
            block_start_string = "<#",
            block_end_string = "#>",
            variable_start_string = "<@",
            variable_end_string = "@>",
            comment_start_string = "/BB*",
            comment_end_string = "*BB/",
            undefined=jinja2.StrictUndefined,
        )

    def _raw_render(self, variables, body):
        return self.jinja_environment.from_string(str(body)).render(vars=variables)
        

