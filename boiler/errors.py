# root exception
class BoilerError(Exception): pass

# Category Errors
class ProjectError(BoilerError): pass
class ConfigError(BoilerError): pass
class RequestError(BoilerError): pass
class TemplateError(BoilerError): pass

# Project Related Errors
class NotAProjectError(ProjectError): pass
class ProjectExistsError(ProjectError): pass
class TemplateAlreadyAppliedError(ProjectError, RequestError): pass

# Template Related Errors
class CircularDependError(TemplateError, ConfigError): pass
class NoSuchTemplateError(TemplateError, RequestError): pass
class UnreferencedDependBlockError(TemplateError, ConfigError): pass
class UndefinedBlockRequiredError(TemplateError, ConfigError): pass

class ExportWithoutTypeError(TemplateError, ConfigError): pass
class ExportWithBadTypeError(TemplateError, ConfigError): pass
class BlockExportWithoutLocationError(TemplateError, ConfigError): pass
class FolderExportWithoutLocationError(TemplateError, ConfigError): pass
class BlockNotFoundError(TemplateError, ConfigError): pass
class DanglingBlockError(TemplateError, ConfigError): pass

class UnmetRequireError(TemplateError, RequestError): pass
class UnmetDependError(TemplateError, RequestError): pass
