import os
import sh
import pickle

import boiler.errors
import boiler.render
import boiler.template_tree
import boiler.template_fetcher

class Project(object):

    def __init__(self, project_config):
        self.tree = project_config.tree
        self.template_source = project_config.template_source
        self.location = project_config.location
        self.index = project_config.index

        self.config = project_config

    def _build_absolute_path(self, path):
        if path.startswith('/'):
            return os.path.join(self.location, path.lstrip('/'))
        else:
            return os.path.join(self.location, self.index.path.lstrip('/'), path)

    def _build_relative_path(self, path):
        abs_path = os.path.abspath(path).rstrip('/')
        location = self.location.rstrip('/')
        relative = abs_path.replace(location, '')

        if not len(relative):
            return '/'
        return relative.rstrip('/')

    def apply(self):
        if not self.index:
            print("WARN: Nothing in index!")
            return

        for template in self.index.changes():
            self.apply_change(template)

        self.index.apply()
        self._set_index(None)

    def stage(self, path, template_type, template_name=None):
        path = self._build_relative_path(path)

        # Build a changeTree here for the change
        change_tree = boiler.template_tree.TemplateChangeTree.attach(self.tree, path)

        root_template = self.stage_template(change_tree, template_type, template_name)
        change_tree.register_final_target(root_template)

        self._set_index(change_tree)

    def _set_index(self, index):
        self.index = index
        self.config.index = index
        self.config.save()

    def stage_template(self, change_tree, template_type, template_name, seen_depends=None):
        if not seen_depends:
            seen_depends = []

        template = self.template_source.get(template_type, template_name)

        if change_tree.has_template(change_tree.resolve_path, template_type, template_name) \
            or self.tree.has_template(change_tree.resolve_path, template_type, template_name):

            raise boiler.errors.TemplateAlreadyAppliedError("Template %s has already been applied to project" % template.ident)

        deps = template.dependencies
        for dep in deps:
            if dep.name in seen_depends:
                raise boiler.errors.CircularDependError("Circular dependency detected found processing %s. Templates Involved: %s" % (template.name, seen_depends))

            if not change_tree.has_template(change_tree.resolve_path, dep.type, dep.name):
                seen_depends.append(dep.name)

                try:
                    self.stage_template(change_tree, dep.type, dep.name)
                except boiler.errors.NoSuchTemplateError as e:
                    args = list(e.args)
                    args[0] += " - Required by %s" % template.ident
                    e.args = tuple(args)
                    raise

                seen_depends.pop()

        location_render = change_tree.get_render_object(boiler.render.PythonFormatStringRender)

        template.source.resolved = str(template.source)

        rendered_destination = location_render.render(template.destination)
        relative_destination = self._build_relative_path(rendered_destination)

        template.destination.resolved = self._build_absolute_path(relative_destination)

        change_tree.add_template(template, relative_destination)

        return template

    def apply_change(self, template):
        source = template.source.resolved
        destination = template.destination.resolved

        if source[-1] != "/":
            source += "/"

        print("Do Apply", template.ident, source, "->", destination)

        if os.path.exists(source):
            sh.rsync("-a", source, destination)

        if not len(template.blocks):
            return

        block_render = self.index.get_render_object(boiler.render.JinjaStringRender)
        for block in template.blocks:
            path, export = self.index.satisfy_export(block.name)

            block_location = self._build_absolute_path(os.path.join(path.lstrip("/"), export.location))
            try:
                with open(block_location, 'r') as f:
                    target_body = f.read()
            except IOError:
                raise boiler.errors.BlockNotFoundError("Block %s is defined for file %s but file does not exist" % (block.name, block_location))

            rendered_body = block.render(target_body, block_render, self.index.satisfy_export, block_location)
            with open(block_location, 'w') as f:
                f.write(rendered_body)

class ProjectConfig(object):

    def _find_boiler_cfg(self, location, last=None):
        if os.path.exists(os.path.join(location, ".boiler")):
            return location

        if location == "/" or last == location:
            raise boiler.errors.NotAProjectError("Could not find project root directory")

        return self._find_boiler_cfg(os.path.dirname(location), location)
    
    def __init__(self, settings, location, is_new):
        location = os.path.abspath(location).rstrip('/')
        if is_new:
            self.location = location
        else:
            try:
                self.location = self._find_boiler_cfg(location)
            except boiler.errors.NotAProjectError:
                raise boiler.errors.NotAProjectError("Trying to load folder %s which is not a boiler project" % location)

        self.tree_file = os.path.abspath(os.path.join(self.location, ".boiler"))
        self.index_file = os.path.abspath(os.path.join(self.location, ".boiler.idx"))

        if is_new and os.path.exists(self.tree_file):
            raise boiler.errors.ProjectExistsError("Trying to create project in folder %s which is already a boiler project" % self.location)

        self.template_source = boiler.template_fetcher.TemplateFetcher(settings['flag_paths'] + settings['base_path'])

        if is_new:
            self.tree = boiler.template_tree.TemplateDependencyTree()
            self.index = None
            self.save()
        else:
            with open(self.tree_file, 'rb') as f:
                self.tree = pickle.load(f)

            try:
                with open(self.index_file, 'rb') as f:
                    self.index = pickle.load(f)
            except IOError:
                self.index = None

    def save(self):
        with open(self.tree_file, 'wb') as f:
            pickle.dump(self.tree, f)

        if self.index:
            with open(self.index_file, 'wb') as f:
                pickle.dump(self.index, f)
        else:
            try:
                os.remove(self.index_file)
            except OSError:
                pass

