import re
import os
import configparser

import boiler.common
import boiler.errors
import boiler.consts

class Template(object):

    def __init__(self, config, name):
        self.type = config.type
        self.name = name

        if self.name:
            self.ident = self.name
        else:
            self.ident = self.type

        self.dependencies = config.get_depends()
        self.destination = config.get_destination()
        self.source = config.get_source()
        self.requires = config.get_requires()
        self.exports = config.get_exports()
        self.blocks = config.get_blocks()

    def __str__(self):
        return """
            Template %s (%s):
            Deps: %s
            Requires: %s
            Exports: %s
            Source: %s
            Destination: %s
        """ % (
            self.name, 
            self.type, 
            self.dependencies, 
            self.requires, 
            self.exports, 
            self.source, 
            self.destination
        )

class TemplateConfig(object):
    """
    Lower level class that is aware of how the config file is formatted
    and how the filesystem is layed out.

    It builds all the required classes that a Template object requires.

    This class enforces things such as validity of the config and the template folder.
    """

    def __init__(self, template_type, template_dir, cfg_file):
        self.type = template_type
        self.folder = template_dir
        self.cfg_file = cfg_file

        assert os.path.exists(self.folder)
        assert os.path.exists(self.cfg_file)

        self.config = configparser.RawConfigParser(allow_no_value=True)
        self.config.read(self.cfg_file)

        self.deps = {}
        self.requires = {}
        self.blocks = []
        self.exports = {}

        for section in self.config.sections():
            valid = False
            for valid in boiler.consts.VALID_SECTIONS:
                if valid.match(section) is not None:
                    valid = True
                    break
            if not valid:
                print("WARN: Unrecognised section %s in template %s" % (section, self.type))

        if self.config.has_section("requires"):
            for name, require_type in self.config.items("requires"):
                if require_type == "block":
                    self.blocks.append(boiler.common.Block(self.type, self.folder, name))
                else:
                    self.requires[name] = boiler.common.Requirement(name, require_type)

        if self.config.has_section("depends"):
            for dep_type, dep_name in self.config.items("depends"):
                options = None
                if self.config.has_section("depend:%s" % dep_name):
                    options = {key: val for key, val in self.config.items("depend:%s" % dep_name)}
                else:
                    options = {}
                dep = boiler.common.Dependency(dep_type, dep_name, options)
                self.deps[dep.ident] = dep

        for section in self.config.sections():
            if section.startswith("export:"):
                export_name = section.split(":", 1)[1]
                options = {key: val for key, val in self.config.items(section)}

                self.exports[export_name] = boiler.common.Export.build(export_name, options)

            if section.startswith("depend:"):
                dep_ident = section.split(":", 1)[1]

                if dep_ident not in self.deps:
                    raise boiler.errors.UnreferencedDependBlockError("Template %s does not list %s in depends but there is a depend block for it" % (self.type, dep_name))

        source = {"source_type": None, "target": self.folder}
        destination = {"destination_type": None, "target": None}

        if self.config.has_section("location"):
            if self.config.has_option("location", "source"):
                source_raw = re.split(r"\s+", self.config.get("location", "source"), 1)

                source['source_type'] = source_raw[0]
                if len(source_raw) > 1:
                    source['target'] = source_raw[1]

            if self.config.has_option("location", "destination"):
                destination_raw = re.split(r"\s+", self.config.get("location", "destination"), 1)

                destination['destination_type'] = destination_raw[0]
                if len(destination_raw) > 1:
                    destination['target'] = destination_raw[1]

        self.source = boiler.common.Source(**source)
        self.destination = boiler.common.Destination(**destination)

    def get_destination(self):
        return self.destination

    def get_source(self):
        return self.source

    def get_requires(self):
        return self.requires

    def get_depends(self):
        return list(self.deps.values())

    def get_exports(self):
        return self.exports

    def get_blocks(self):
        return self.blocks

