import os

import boiler.errors
import boiler.template

class TemplateFetcher(object):

    def __init__(self, locations=None, TemplateClass=boiler.template.Template, TemplateConfigClass=boiler.template.TemplateConfig):
        self.locations = []

        for location in locations:
            self.add_location(location)

        # this makes unit testing much easier
        self.TemplateClass = TemplateClass
        self.TemplateConfigClass = TemplateConfigClass

    def add_location(self, location):
        if location:
            self.locations.append(os.path.expanduser(str(location)))

    def get_paths(self, dotted_template_type):
        dir_type = dotted_template_type.replace(".", "/")

        for location in self.locations:
            template_dir = os.path.join(location, dir_type)
            cfg_file = os.path.join(template_dir, "boiler.cfg")

            if os.path.exists(cfg_file):
                return template_dir, cfg_file

        raise boiler.errors.NoSuchTemplateError("Template %s cannot be found" % dotted_template_type)

    def get(self, template_type, template_name):
        template_dir, cfg_file = self.get_paths(template_type)
        template_config = self.TemplateConfigClass(template_type, template_dir, cfg_file)

        return self.TemplateClass(template_config, template_name)

    def __str__(self):
        return super(TemplateFetcher, self).__str__() + " " + str(self.locations)
