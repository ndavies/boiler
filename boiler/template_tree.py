import functools

import boiler.errors

# Category level exception
class TemplateTreeError(boiler.errors.BoilerError): pass
class TemplateDependencyTreeError(TemplateTreeError): pass
class TemplateChangeTreeError(TemplateDependencyTreeError): pass

# You cannot add the same template to the same path twice
class DuplicateTemplateError(TemplateTreeError): pass

# Some functions can only be called on the root node
class RootNodeOnly(TemplateTreeError): pass

# Could not meet a dependency
class UnmetDependencyError(TemplateDependencyTreeError): pass
class UnmetExportError(TemplateDependencyTreeError): pass

class TargetTemplateNotSet(TemplateChangeTreeError): pass

class TemplateTree(object):
    def __init__(self, parent=None, path="/"):

        if parent is None:
            self.root = self
            self.registered_by_name = {}
        else:
            self.root = parent.root

        self.parent = parent
        self.children = {}
        self.path = path

        self.templates = []
        self.templates_by_name = {}
        self.templates_by_type = {}

    def parents(self):
        """
        A generator for the current node then all the nodes above it up the tree
        """
        node = self
        yield node
        while node.parent:
            node = node.parent
            yield node

    def find(self, path):
        """
        Find the deepest node that contains given path.
        
        returns: (node, bool) where node is the matched node and bool is if its an exact match
        """

        # Exact match
        if self.path == path:
            return self, True

        for child_path, child in self.children.items():
            # If there is a deeper child that matches path then use that
            if path.startswith(child_path):
                return child.find(path)

            # If there is a child with a more specific path than target then we are done
            if child_path.startswith(path):
                return self, False
        return self, False

    def has_children(self):
        """
        Returns if the node has any children or not
        """
        return len(self.children) != 0

    def add_child(self, path, child):
        """
        Adds a child to the node

        Note: Always use this function (even internally to the class) because 
        TemplateDependencyTree needs to keep its cache consistent.
        """
        self.children[path] = child

    def remove_child(self, path):
        """
        removes a child from the node

        Note: Always use this function (even internally to the class) because 
        TemplateDependencyTree needs to keep its cache consistent.
        """
        del self.children[path]

    def add_template(self, template, path):
        """
        Adds a template at a given path and rearranges the tree if necessary

        This function traverses down the tree and if an node with a path that
        matches exactly is found then the template is added to it.

        If it gets to a leaf node and the path is still below it, it will be
        inserted as a new child.

        If a node is reached with one or more children with paths that are
        deeper than the specified path (eg if path = /var/, the root node has
        path = / and the child has /var/log/) a new node will be inserted and
        all the children that are contained by the target path will be moved
        into the new node.
        """
        node, exact = self.find(path)

        if exact:
            if template.name in node.templates_by_name:
                raise DuplicateTemplateError("Template %s already applied for path %s" % (template.name, path))

            if template.name:
                self.root.register_template(node, template)
                node.templates_by_name[template.name] = template
            else:
                if template.type in node.templates_by_type:
                    raise DuplicateTemplateError("Template of type %s already applied for path %s" % (template.type, path))

            if template.type in node.templates_by_type:
                node.templates_by_type[template.type].append(template)
            else:
                node.templates_by_type[template.type] = [template]

            node.templates.append(template)

            return node

        # For inexact matches always need a new node
        new_child = self.__class__(node, path)
        new_child.add_template(template, path)

        # Copy all of the children that have paths under the new node into its children
        children = node.children.copy()
        for child_path, child in children.items():
            if child_path.startswith(path):
                new_child.add_child(child_path, child)
                node.remove_child(child_path)

        # Add new child to children
        node.add_child(path, new_child)

        return node

    def register_template(self, node, template):
        """
        This is used to register a new template with the root node.

        Throws: RootNodeOnly if called on a non root node
        """
        if self.parent is not None:
            raise RootNodeOnly("register_template can only be called on the root node")

        if template.name:
            if template.name in self.registered_by_name:
                raise DuplicateTemplateError("Template %s already registered - if you wish to add it again use an alias" % (template.name))
            else:
                self.registered_by_name[template.name] = (node, template)

    def has_template(self, path, template_type, template_name):
        if template_name:
            return self.root.has_registered_template(template_name)

        node, exact = self.find(path)
        if exact:
            return (template_type in node.templates_by_type)

        return False

    def has_registered_template(self, template_name):
        if self.parent is not None:
            raise RootNodeOnly("has_template can only be called on the root node")

        return (template_name in self.registered_by_name)

    def __str__(self, depth=0):
        pad = ("   " * depth)

        output = str(self.path)
        if len(self.children) or len(self.templates):
            output += '\n'

        for template in self.templates:
            output += pad + "| %s (%s)" % (template.type, template.name) + "\n"

        for p, child in self.children.items():
            output += pad + "\--" + str(child.__str__(depth+1))

        return output

class TemplateDependencyTree(TemplateTree):
    
    def __init__(self, *args, **kwargs):
        super(TemplateDependencyTree, self).__init__(*args, **kwargs)

        if self.parent is None:
            self.exports = {}
        self.export_cache = {}

    def register_template(self, node, template):
        result = super(TemplateDependencyTree, self).register_template(node, template)
        self.exports.update(template.exports)
        return result

    def remove_child(self, path):
        result = super(TemplateDependencyTree, self).remove_child(path)

        self.export_cache = {}
        for template in self.templates:
            self.export_cache.update(template.exports)

        return result

    def add_template(self, template, *args, **kwargs):
        node = super(TemplateDependencyTree, self).add_template(template, *args, **kwargs)
        node.export_cache.update(template.exports)
        return node

    def satisfy_dependency(self, template_type, path=None, name=None):
        """
        Returns the template object that meets the dependency given by 
        template_type and optionally a name from the perspective 
        of the given path

        First the node that best fits the given path is found.
        Then at each level up the tree it checks to see if there are
        any templates with the given name (if given) and returns instantly
        if one is found.

        It also records the first found match for the template_type on its
        way up the tree.

        Once it reaches the root node it will search for templates with the
        given name (if given) in the whole tree and return that. If 
        no template with that name but a template_type match is found it 
        will return that match otherwise it will raise UnmentDependencyError
        """

        if path is None:
            path = self.path

        if self.parent is not None:
            raise RootNodeOnly("satisfy_dependency can only be called on the root node")

        base_node, exact = self.find(path)

        type_match = None
        for node in base_node.parents():

            if name in node.templates_by_name:
                return node, node.templates_by_name[name]

            if not type_match and template_type in self.templates_by_type:
                # If there is a match get the latest added match
                type_match = node, self.templates_by_type[template_type][-1]

                # If there is no name the first template_type is good enough
                if not name:
                    return type_match

        if name in self.root.registered_by_name:
            return self.root.registered_by_name[name]

        if type_match:
            return type_match

        raise UnmetDependencyError("Could not satisfy dependency %s (%s) from path %s" % (template_type, name, path))

    def satisfy_export(self, name, path=None):
        """
        Returns the export that matches the given name for the given path

        The exports are recursivly searched and the first match is returned
        If not match is found a UnmetExportError is raised
        """
        if path is None:
            path = self.path

        if self.parent is not None:
            raise RootNodeOnly("satisfy_export can only be called on the root node")
        
        base_node, exact = self.find(path)

        for node in base_node.parents():
            if name in node.export_cache:
                return node.path, node.export_cache[name]
        
        raise UnmetExportError("Could not satisfy export %s from path %s" % (name, path))

class TemplateChangeTree(TemplateDependencyTree):
    """
    This object holds the state of a new tree of templates
    that is going to be added to a tree.

    First you call stage (template)

    and when you are finished you call apply which will
    add all the templates in the ChangeTree to the tree
    it is bound to
    """

    @classmethod
    def attach(cls, tree, path):
        """
        We cannot overwrite __init__ because it is called from witing the tree to construct
        new nodes dynamically when you call add. This means that we must use this less clean
        way of building a new change tree.
        """
        change_tree = cls()

        # The tree to attach to
        change_tree.tree = tree

        # The base template that is the root of the change
        change_tree.target = None

        # The path to resolve variables/dependencies
        change_tree.resolve_path = path

        return change_tree

    def apply(self, tree=None):
        if tree is None:
            if self.parent is not None:
                raise RootNodeOnly("apply can only be called on the root node")
            tree = self.tree

        for template in self.templates:
            tree.add_template(template, self.path)

        for path, child in self.children.items():
            child.apply(tree)

    def register_final_target(self, template):
        self.target = template

    def get_render_object(self, RenderClass, options=None):
        var_source = functools.partial(self.satisfy_export, path=self.resolve_path)

        return RenderClass(var_source, UnmetExportError)
        
    def satisfy_dependency(self, template_type, path=None, name=None):
        if not path:
            path = self.resolve_path

        return super(TemplateChangeTree, self).satisfy_dependency(template_type, path, name)

    def satisfy_export(self, name, path=None):
        my_exception = None
        my_path = None
        my_export = None

        their_exception = None
        their_path = None
        their_export = None

        try:
            my_path, my_export = super(TemplateChangeTree, self).satisfy_export(name, path)
        except UnmetExportError as e:
            my_exception = e

        try:
            their_path, their_export = self.tree.satisfy_export(name, self.resolve_path)
        except UnmetExportError as e:
            their_exception = e

        # change tree has it
        if my_exception is None and their_exception is not None:
            return my_path, my_export

        # change tree does not have it
        if my_exception is not None and their_exception is None:
            return their_path, their_export

        # both have it
        if my_exception is None and their_exception is None:
            if len(my_path) > len(their_path):
                return my_path, my_export
            return their_path, their_export

        raise my_exception

    def has_template(self, *args, **kwargs):
        '''
        We have the template if either the tree were attached to has it
        or we have it ourselves
        '''

        return super(TemplateChangeTree, self).has_template(*args, **kwargs)\
            or self.tree.has_template(*args, **kwargs) 

    def changes(self, template=None):
        if not template:
            if not self.target:
                raise TargetTemplateNotSet("You must call register_final_target before asking for changes")
            template = self.target

        for dep in template.dependencies:
            if super(TemplateChangeTree, self).has_template(self.resolve_path, dep.type, dep.name):
                node, dep_template = self.root.satisfy_dependency(dep.type, name=dep.name, path=self.resolve_path)
                for change in self.changes(dep_template):
                    yield change
        yield template

