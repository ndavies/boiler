import os

__version__ = '0.0.1'

def init(project, args):
    print("Built project in", args.project_dir)

    if args.template:
        print("Applying", args.template)
        apply(project, args)

def apply(project, args):
    if args.template:
        stage(project, args)

    project.apply()

def stage(project, args):
    project.stage(os.getcwd(), args.template, args.name)

def status(project, args):
    if project.index:
        print("Staged:")
        print(project.index)
    else:
        print("There is nothing in the index")

