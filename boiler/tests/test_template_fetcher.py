import sys
import os
import functools

from nose.tools import assert_equal
from nose.tools import assert_not_equal
from nose.tools import assert_raises

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../")

from template_fetcher import TemplateFetcher
import errors
from common_test_objects import *

class BaseTemplateFetcherTest(object):
    
    def __init__(self):
        self.tmp_dirs = []

    def teardown(self):
        for d in self.tmp_dirs:
            d.cleanup()

    def new_dir(self):
        d = TmpDir()
        self.tmp_dirs.append(d)
        return d

    def fetcher(self, locations):
        return TemplateFetcher(locations, FakeTemplate, FakeTemplateConfig)


class TestFetcher(BaseTemplateFetcherTest):

    def test_no_dirs(self):
        f = self.fetcher([])

        assert_raises(errors.NoSuchTemplateError, f.get, 'a', 'name')
        assert_raises(errors.NoSuchTemplateError, f.get, 'b', 'name')
        assert_raises(errors.NoSuchTemplateError, f.get, 'b.c', 'name')
        assert_raises(errors.NoSuchTemplateError, f.get, 'b/c', 'name')

    def test_single_dir(self):
        d = self.new_dir()
        template_dirs = d.make_templates([
            'a',
            'a.b',
            'a.a',
        ])
    
        f = self.fetcher([d])
        t = f.get('a', 'name')

        assert_equal('name', t.name)
        assert_equal(os.path.join(str(d), 'a'), t.cfg.template_dir)
        assert_equal(t.cfg.template_dir, str(template_dirs[0]))

        f = self.fetcher([d])
        t = f.get('a.b', 'name1')
        assert_equal('name1', t.name)
        assert_equal(os.path.join(str(d), 'a/b'), t.cfg.template_dir)
        assert_equal(t.cfg.template_dir, str(template_dirs[1]))

        t = f.get('a.a', 'name2')
        assert_equal('name2', t.name)
        assert_equal(os.path.join(str(d), 'a/a'), t.cfg.template_dir)
        assert_equal(t.cfg.template_dir, str(template_dirs[2]))

        assert_raises(errors.NoSuchTemplateError, f.get, 'c', 'name2')
        assert_raises(errors.NoSuchTemplateError, f.get, 'c.d', 'name3')

    def test_two_dirs(self):
        d1 = self.new_dir()
        template1_dirs = d1.make_templates(['a', 'b', 'a.c'])

        d2 = self.new_dir()
        template2_dirs = d2.make_templates(['a', 'c', 'd.e'])
    
        f = self.fetcher([d1, d2])

        t = f.get('a', 'name')
        assert_equal('name', t.name)
        assert_equal(os.path.join(str(d1), 'a'), t.cfg.template_dir)
        assert_equal(t.cfg.template_dir, str(template1_dirs[0]))
        
        t = f.get('b', 'name')
        assert_equal(t.cfg.template_dir, str(template1_dirs[1]))

        t = f.get('a.c', 'name')
        assert_equal(t.cfg.template_dir, str(template1_dirs[2]))

        t = f.get('c', 'name')
        assert_equal(t.cfg.template_dir, str(template2_dirs[1]))

        t = f.get('d.e', 'name')
        assert_equal(t.cfg.template_dir, str(template2_dirs[2]))

    def test_dynamic_dirs(self):
        d1 = self.new_dir()
        template1_dirs = d1.make_templates(['a', 'b', 'a.c'])

        f = self.fetcher([d1])

        t = f.get('a', 'name')
        assert_equal('name', t.name)
        assert_equal(os.path.join(str(d1), 'a'), t.cfg.template_dir)
        assert_equal(t.cfg.template_dir, str(template1_dirs[0]))
        
        t = f.get('b', 'name')
        assert_equal(t.cfg.template_dir, str(template1_dirs[1]))

        t = f.get('a.c', 'name')
        assert_equal(t.cfg.template_dir, str(template1_dirs[2]))

        d2 = self.new_dir()
        f.add_location(d2)
        template2_dirs = d2.make_templates(['a', 'c', 'd.e'])

        t = f.get('a', 'name')
        assert_equal('name', t.name)
        assert_equal(os.path.join(str(d1), 'a'), t.cfg.template_dir)
        assert_equal(t.cfg.template_dir, str(template1_dirs[0]))
    
        t = f.get('c', 'name')
        assert_equal(t.cfg.template_dir, str(template2_dirs[1]))

        t = f.get('d.e', 'name')
        assert_equal(t.cfg.template_dir, str(template2_dirs[2]))

