import sys
import os

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../")

from template_tree import TemplateTree

class FakeTemplate(object):
    def __init__(self, name, path):
        self.name = name
        self.path = path
        self.template_type = "fake"

    def __str__(self):
        print(id(self), self.name, self.path)

class TestClass:

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_add_template(self):
        tt = TemplateTree()

        tt.add_template(FakeTemplate(name="t1", path="/"), "/")
        tt.add_template(FakeTemplate(name="t2", path="/"), "/")
        tt.add_template(FakeTemplate(name="t3", path="/b"), "/b")
        tt.add_template(FakeTemplate(name="t4", path="/b/c"), "/b/c")
        tt.add_template(FakeTemplate(name="t5", path="/b/c/d"), "/b/c/d")
        tt.add_template(FakeTemplate(name="t6", path="/b/e/f"), "/b/e/f")
        tt.add_template(FakeTemplate(name="t7", path="/b/e/g"), "/b/e/g")
        tt.add_template(FakeTemplate(name="t8", path="/b/e"), "/b/e")

        print("root")
        print(tt)

        assert False
