import os
import shutil
import tempfile

class FakeTemplateConfig(object):
    
    def __init__(self, template_type, template_dir, cfg_file):
        self.template_type = template_type
        self.template_dir = template_dir
        self.cfg_file = cfg_file

class FakeTemplate(object):

    def __init__(self, cfg, name):
        self.cfg = cfg
        self.name = name

class TmpDir(object):

    def __init__(self):
        self.d = tempfile.mkdtemp()
        print("new dir", self.d)

    def cleanup(self):
        shutil.rmtree(self.d)

    def make_template(self, name):
        if "/" in name:
            raise Exception("Cannot have / in template name!")
        if name[0] == ".":
            raise Exception("Cannot have . as first character in template name!")

        path = os.path.join(self.d, name.replace(".", "/"))
        os.makedirs(path)

        with open(os.path.join(path, 'boiler.cfg'), 'w') as f:
            f.write('')

        return path

    def make_templates(self, names):
        result = []
        for name in names:
            result.append(self.make_template(name))

        return result

    def __str__(self):
        return self.d


class_map = {
    'template': FakeTemplate,
    'templateConfig': FakeTemplateConfig
}
