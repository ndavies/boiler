import sys
import os
import functools
from nose.tools import assert_equal
from nose.tools import assert_not_equal
from nose.tools import assert_raises

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../")

from render import VariableSource
from render import PythonFormatStringRender
from render import JinjaStringRender

class TestVariableSource(object):
    
    class ValidTestException(Exception): pass
    class InvalidTestException(Exception): pass

    options = set(['a','b','c'])

    def valid_variable_source(self, options, die, throw_valid, item):
        if item in options:
            return "OK_" + item
        else:
            if die:
                1/0
            if throw_valid:
                raise self.ValidTestException("Could not get " + item)
            else:
                raise self.InvalidTestException("Could not get " + item)
    
    def test_good_v(self):
        good_f = functools.partial(self.valid_variable_source, self.options, False, True)
        good_v = VariableSource(good_f, self.ValidTestException)

        for option in self.options:
            assert_equal(getattr(good_v, option), "OK_" + option)
            assert_equal(good_v[option], "OK_" + option)

        assert_equal(good_v._get_fails(), set([]))
        try:
            good_v._raise_any_errors()
            assert True
        except self.ValidTestException:
            assert False

        try:
            good_v.x
            good_v['y']
            assert True
        except self.ValidTestException:
            assert False

        assert_equal(good_v._get_fails(), set(['x', 'y']))

        try:
            good_v._raise_any_errors()
            assert False
        except self.ValidTestException:
            assert True

    def test_bad_v(self):
        failing_f = functools.partial(self.valid_variable_source, self.options, False, False)
        failing_v = VariableSource(failing_f, self.InvalidTestException)

        try:
            failing_v.x
            failing_v['y']
            assert True
        except self.ValidTestException:
            assert False

        assert_equal(failing_v._get_fails(), set(['x', 'y']))

        try:
            failing_v._raise_any_errors()
            assert False
        except self.InvalidTestException:
            assert True

    def test_die_v(self):
        die_f = functools.partial(self.valid_variable_source, self.options, True, False)
        die_v = VariableSource(die_f, self.ValidTestException)

        assert_equal(die_v.a, "OK_a")
        try:
            die_v['a']
            die_v.b
            assert True
        except self.ValidTestException:
            assert False

        try:
            die_v.z
            assert False
        except self.ValidTestException:
            assert False
        except ZeroDivisionError:
            assert True

        try:
            die_v['x']
            assert False
        except self.ValidTestException:
            assert False
        except ZeroDivisionError:
            assert True

class DataToStrObject(object):
    
    def __init__(self, data):
        self.data = data

    def __str__(self):
        return str(self.data)


class BaseStringRenderTest(object):

    class ValidTestException(Exception): pass
    class InvalidTestException(Exception): pass

    options = set(['a','b','c'])

    def __init__(self):
        self.basic_object_input = DataToStrObject(self.basic_input)

    def valid_variable_source(self, options, die, throw_valid, item):
        if item in options:
            return "OK_" + item
        else:
            if die:
                1/0
            if throw_valid:
                raise self.ValidTestException("Could not get " + item)
            else:
                raise self.InvalidTestException("Could not get " + item)

    def test_good_v(self):
        good_f = functools.partial(self.valid_variable_source, self.options, False, True)
        good_render = self.TestClass(good_f, self.ValidTestException)

        assert_equal(good_render.render(self.basic_input), self.basic_output)
        assert_equal(good_render.render(self.basic_input), self.basic_output)

        assert_equal(good_render.render(self.basic_object_input), self.basic_output)

        try:
            good_render.render(self.bad_input)
            assert False
        except self.ValidTestException as e:
            assert_equal(len(e.fails.keys()), 1)
            assert_equal(type(e.fails[list(e.fails.keys())[0]]), self.ValidTestException)

        assert_equal(good_render.render(self.basic_input), self.basic_output)
        
    def test_bad_v(self):
        failing_f = functools.partial(self.valid_variable_source, self.options, False, False)
        failing_render = self.TestClass(failing_f, self.InvalidTestException)

        assert_equal(failing_render.render(self.basic_input), self.basic_output)
        assert_equal(failing_render.render(self.basic_input), self.basic_output)

        try:
            failing_render.render(self.bad_input)
            assert False
        except self.ValidTestException as e:
            assert False
        except self.InvalidTestException as e:
            assert_equal(len(e.fails.keys()), 1)
            assert_equal(type(e.fails[list(e.fails.keys())[0]]), self.InvalidTestException)

        assert_equal(failing_render.render(self.basic_input), self.basic_output)

    def test_die_v(self):
        die_f = functools.partial(self.valid_variable_source, self.options, True, False)
        die_render = self.TestClass(die_f, self.ValidTestException)

        assert_equal(die_render.render(self.basic_input), self.basic_output)
        assert_equal(die_render.render(self.basic_input), self.basic_output)

        try:
            die_render.render(self.bad_input)
            assert False
        except self.ValidTestException as e:
            assert False
        except ZeroDivisionError as e:
            assert True

        assert_equal(die_render.render(self.basic_input), self.basic_output)

class TestPythonFormatStringRender(BaseStringRenderTest):
    
    basic_input = "start %(a)s %(b)s %(c)s end"
    basic_output = "start OK_a OK_b OK_c end"

    bad_input = "start %(a)s %(b)s %(c)s %(d)s end"
    
    TestClass = PythonFormatStringRender
    
class TestPythonFormatStringRender(BaseStringRenderTest):

    basic_input = """start <@ vars.a @> <@ vars.b @> <@ vars.c @> end <# if vars.a == "OK_a" #>TEST<# endif #> {{ vars.d }}"""
    basic_output = "start OK_a OK_b OK_c end TEST {{ vars.d }}"

    bad_input = "start <@ vars.a @> <@ vars.b @> <@ vars.c @> <@ vars.d @> end"
    
    TestClass = JinjaStringRender
