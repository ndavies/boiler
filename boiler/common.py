import os
import re
import collections

import boiler.errors

##############################
## Common Classes           ##
##############################
class Export(object):

    @classmethod
    def build(cls, name, options):
        if "type" not in options:
            raise errors.ExportWithoutTypeError("export %s has no type field" % name)

        export_type = options['type']
        del options['type']

        if export_type not in export_types:
            raise errors.ExportWithBadTypeError("export %s has invalid type %s" % (name, export_type))

        return export_types[export_type](name, options)
    
    def __init__(self, name, options):
        self.name = name
        self.options = options

class BlockExport(Export):

    def __init__(self, name, options):
        if "location" not in options:
            raise errors.BlockExportWithoutLocationError("Block %s has no location field" % name)

        self.location = options['location']
        del options['location']
        super(BlockExport, self).__init__(name, options)

    def __str__(self):
        return self.location

class FolderExport(Export):

    def __init__(self, name, options):
        if "location" not in options:
            raise errors.FolderExportWithoutLocationError("Block %s has no location field" % name)

        self.location = os.path.abspath(options['location'])
        del options['location']
        super(FolderExport, self).__init__(name, options)

    def __str__(self):
        return self.location

export_types = {
    "folder": FolderExport,
    "block": BlockExport
}

class Block(object):
    
    def __init__(self, template_type, template_folder, name):
        self.template_type = template_type
        self.template_folder = template_folder
        self.name = name
        self.body = None

        if not os.path.join(self.template_folder, "blocks", self.name):
            raise errors.UndefinedBlockRequiredError("Template %s requires block %s but does not define its contents" % (self.template_type, self.name)) 

    def load(self):
        if self.body:
            return

        with open(os.path.join(self.template_folder, "blocks", self.name)) as f:
            self.body = f.read()

    def render(self, inbound_body, renderer, export_source, block_location):
        target_sections = re.split("([ \t]*)(.*BB:START\(%s\).*\n)" % self.name, inbound_body, 1)

        if len(target_sections) != 4:
            raise errors.BlockNotFoundError("Block %s is defined for file %s but it cannot be found" % (self.name, block_location))

        target_sections = target_sections[:-1] + re.split("(.*BB:END)", target_sections[-1], 1)
        if len(target_sections) != 6 or re.search("BB:START\([A-Za-z0-9_]*\)", target_sections[3]) is not None:
            raise errors.DanglingBlockError("Block %s in %s started but not ended correctly" % (self.name, block_location))

        padding = target_sections[1]
        padded_block_body = ""
        for line in renderer.render(self).split("\n"):
            if line.strip() != "":
                line = padding + line + "\n"
            padded_block_body += line
        target_sections.insert(4, padded_block_body)

        return "".join(target_sections)

    def __str__(self):
        self.load()
        return self.body

class Requirement(object):
    
    def __init__(self, name, require_type):
        self.name = name
        self.require_type = require_type

class Dependency(object):
    
    def __init__(self, template_type, template_name, options):
        self.type = template_type
        self.name = template_name
        self.options = options

        if self.name:
            self.ident = self.name
        else:
            self.ident = self.type

class Source(object):

    default_source_type = "local"

    def __init__(self, source_type, target):
        if source_type is None:
            source_type = self.default_source_type

        self.source_type = source_type
        self.target = target

        if self.source_type != "local":
            raise NotImplementedError("Only local is valid for now")

    def __str__(self):
        return os.path.join(self.target, 'payload')

class Destination(object):

    default_destination_type = "local"
    default_target = ''

    def __init__(self, destination_type, target):
        if destination_type is None:
            destination_type = self.default_destination_type

        if target is None:
            target = self.default_target

        self.destination_type = destination_type
        self.target = target
        self.resolved = None

        if self.destination_type != "static":
            raise NotImplementedError("Only Static is valid for now")

    def __str__(self):
        return self.target

