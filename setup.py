import setuptools

import boiler

setuptools.setup(
    name='boiler',
    version=boiler.__version__,
    url='#TODO',
    license='#TODO',
    author='Nick Davies',
    tests_require=['pytest'],
    scripts = ['bin/boiler'],
    install_requires=[
        'jinja2',
        'sh',
    ],
    author_email='git@nicolasdavies.com.au',
    description='Modular Boilerplate code management',
    long_description='#TODO',
    packages=['boiler'],
    platforms='any',
    test_suite='#TODO',
)
# file found at http://www.jeffknupp.com/blog/2013/08/16/open-sourcing-a-python-project-the-right-way/
